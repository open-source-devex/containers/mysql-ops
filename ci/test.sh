#!/usr/bin/env sh
# shellcheck shell=dash

set -euo pipefail
# Note: \n can not be last, or it will be stripped by $()
IFS=$(printf ' \n\t')


docker rm -f "${CONTAINER_NAME}" 2>&1 > /dev/null || true

docker run --name "${CONTAINER_NAME}" \
  --entrypoint ash \
  -dt "${CONTAINER_TEST_IMAGE}"

timeout 15 docker logs -f "${CONTAINER_NAME}" || true

# tests
docker exec -t "${CONTAINER_NAME}" ash -c 'type aws'
docker exec -t "${CONTAINER_NAME}" ash -c 'type mysqldump'
docker exec -t "${CONTAINER_NAME}" ash -c 'type mysql'

# clean up
docker rm -f "${CONTAINER_NAME}"
