#!/usr/bin/env sh
# shellcheck shell=dash

set -euo pipefail
# Note: \n can not be last, or it will be stripped by $()
IFS=$(printf ' \n\t')

if [ -z "${CONTAINER_TEST_IMAGE:-}" ]; then
  CONTAINER_TEST_IMAGE=test-image
fi

set -v

docker build -t ${CONTAINER_TEST_IMAGE} container
docker push ${CONTAINER_TEST_IMAGE}
