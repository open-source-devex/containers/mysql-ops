#!/usr/bin/env ash
# shellcheck shell=dash

set -euo pipefail
# Note: \n can not be last, or it will be stripped by $()
IFS=$(printf ' \n\t')

timestamp="$( date -u +%Y%m%d_%H%M%S )"
dump_file="/tmp/db-dump-${timestamp}.sql"
archive_file="/tmp/${ARCHIVE_NAME:-db-dump-${timestamp}.tar.gz}"

# Send message as output of handler function and to execution log
say() {
  echo "==> ${*}"
}

error() {
  echo "##> ${*}"
}

aws_cmd() {
  # shellcheck disable=SC2068
  aws ${AWS_ENDPOINT_OVERRIDE:+--endpoint-url $AWS_ENDPOINT_OVERRIDE} $@
}

validate_db_config() {
  if [ -z "${DB_HOST:-}" ]; then
    error "Validate DB config :: expecting \${DB_HOST} defined on the environment."
    exit 1
  fi
  if [ -z "${DB_NAME:-}" ]; then
    error "Validate DB config :: expecting \${DB_NAME} defined on the environment."
    exit 1
  fi
  if [ -z "${DB_USERNAME:-}" ]; then
    error "Validate DB config :: expecting \${DB_USERNAME} defined on the environment."
    exit 1
  fi
  if [ -z "${DB_PASSWORD:-}" ]; then
    error "Validate DB config :: expecting \${DB_PASSWORD} defined on the environment."
    exit 1
  fi
}

validate_s3_bucket() {
  if [ -z "${S3_BUCKET:-}" ]; then
    error "Validate S3 config :: expecting \${S3_BUCKET} defined on the environment."
    exit 1
  fi
}

validate_s3_key_prefix() {
  if [ -z "${S3_KEY_PREFIX:-}" ]; then
    error "Validate S3 config :: expecting \${S3_KEY_PREFIX} defined on the environment."
    exit 1
  fi
}

validate_s3_object_key() {
  if [ -z "${S3_OBJECT_KEY:-}" ]; then
    error "Validate remote archive :: expecting \${S3_OBJECT_KEY} defined on the environment."
    exit 1
  fi
}

validate_dump() {
  if [ ! -f "${dump_file}" ]; then
    error "Validate dump :: expecting ${dump_file} to exist."
    exit 1
  fi
}

validate_local_archive() {
  if [ ! -f "${archive_file}" ]; then
    error "Validate local archive :: expecting ${archive_file} to exist."
    exit 1
  fi
}

validate_downloaded_archive() {
  if [ ! -f "$( local_file_downloaded_archive )" ]; then
    error "Validate local archive :: expecting ${archive_file} to exist."
    exit 1
  fi
}

dump_database() {
  say "Dumping database ${DB_NAME}"

  mysqldump --quick --skip-lock-tables --single-transaction --no-tablespaces --max-allowed-packet=512M \
    "--host=${DB_HOST}" "--user=${DB_USERNAME}" "--password=${DB_PASSWORD}" \
    "${DB_NAME}" > "${dump_file}"
}

compress_dump() {
  validate_dump

  say "Compressing dump ${dump_file} to ${archive_file}"

  tar -czf "${archive_file}" -C /tmp "$( basename "${dump_file}" )"
}

upload() {
  validate_local_archive

  s3_url="s3://${S3_BUCKET}/${S3_KEY_PREFIX}/$( basename "${archive_file}" )"

  say "Uploading dump archive to ${s3_url}"

  aws_cmd s3 cp "${archive_file}" "${s3_url}"
}

local_file_downloaded_archive() {
  echo "/tmp/$( basename "${S3_OBJECT_KEY}" )"
}

local_file_downloaded_dump() {
  tar -tzf "$( local_file_downloaded_archive )"
}

remote_file_url() {
  echo "s3://${S3_BUCKET}/${S3_OBJECT_KEY}"
}

download() {
  s3_url="$( remote_file_url )"

  say "Downloading ${s3_url}"

  aws_cmd s3 cp "${s3_url}" "$( local_file_downloaded_archive )"
}

extract_archive() {
  validate_downloaded_archive

  local_archive="$( local_file_downloaded_archive )"

  say "Extracting archive ${local_archive} to /tmp/$( local_file_downloaded_dump )"

  tar -xzf "${local_archive}" -C /tmp
}

restore_database() {
  say "Restoring database ${DB_NAME}"

  mysql \
    "--host=${DB_HOST}" "--user=${DB_USERNAME}" "--password=${DB_PASSWORD}" \
    "${DB_NAME}" < "/tmp/$( local_file_downloaded_dump )"
}

set -euo pipefail
# Note: \n can not be last, or it will be stripped by $()
IFS=$(printf ' \n\t')

say "AWS principal: $( aws sts get-caller-identity )"

# parse first argument that decides what to do
case "${1:-}" in
  "dump")
    validate_db_config
    validate_s3_bucket
    validate_s3_key_prefix

    dump_database
    compress_dump
    upload
  ;;
  "restore")
    validate_db_config
    validate_s3_bucket
    validate_s3_object_key

    download
    extract_archive
    if restore_database; then
      touch success
      aws s3 cp success "$( remote_file_url ).restored"
    fi
  ;;
  *)
    if [ -z "${1:-}" ]; then
      error "No command given."
    else
      error "Unknown command ${1:-}."
    fi
    exit 0
  ;;
esac;
