# Mysql Ops

A container that implements MySQL db operations.

### DB Dump

The DB dump operation is executed by passing the argument `dump` to the entrypoint script (in docker terminology that would be the command).
The container will dump a database, compress the dump file and upload it to an S3 bucket.

Required environment variables:
- DB_HOST
- DB_NAME
- DB_USERNAME
- DB_PASSWORD
- S3_BUCKET
- S3_KEY_PREFIX
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

Optional environment variables
- AWS_ENDPOINT_OVERRIDE

### DB Restore

The DB dump operation is executed by passing the argument `restore` to the entrypoint script (in docker terminology that would be the command).
The container will download a database dump from S3, decompress it and restore the database.

Required environment variables:
- DB_HOST
- DB_NAME
- DB_USERNAME
- DB_PASSWORD
- S3_BUCKET
- DUMP_S3_KEY
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

Optional environment variables
- AWS_ENDPOINT_OVERRIDE
